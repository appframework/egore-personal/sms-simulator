## [1.0.16](https://gitlab.com/appframework/egore-personal/sms-simulator/compare/1.0.15...1.0.16) (2025-03-10)


### Bug Fixes

* **deps:** update dependency com.googlecode.libphonenumber:libphonenumber to v9 ([a5b755b](https://gitlab.com/appframework/egore-personal/sms-simulator/commit/a5b755bc7169b6d2cd2e32e3878be39381d1fb95))
* **deps:** update quarkus.platform.version to v3.19.2 ([1a52e3f](https://gitlab.com/appframework/egore-personal/sms-simulator/commit/1a52e3ff769477c92c92940e38c6e7e57c7f017d))

## [1.0.15](https://gitlab.com/appframework/egore-personal/sms-simulator/compare/1.0.14...1.0.15) (2025-02-26)


### Bug Fixes

* **deps:** update quarkus.platform.version to v3.18.4 ([c4fcd0f](https://gitlab.com/appframework/egore-personal/sms-simulator/commit/c4fcd0f8a812bd5c1a949f2243b8d58beefcd603))

## [1.0.14](https://gitlab.com/appframework/egore-personal/sms-simulator/compare/1.0.13...1.0.14) (2025-02-19)


### Bug Fixes

* **deps:** update dependency com.googlecode.libphonenumber:libphonenumber to v8.13.55 ([4f49a7e](https://gitlab.com/appframework/egore-personal/sms-simulator/commit/4f49a7e7d8ebf7ab270daaa0925fa598bf39d199))
* **deps:** update quarkus.platform.version to v3.18.3 ([cf6c043](https://gitlab.com/appframework/egore-personal/sms-simulator/commit/cf6c04363df495b9d3610d75a0b6b9f4e0ff4474))

## [1.0.13](https://gitlab.com/appframework/egore-personal/sms-simulator/compare/1.0.12...1.0.13) (2025-02-11)


### Bug Fixes

* **deps:** update dependency com.googlecode.libphonenumber:libphonenumber to v8.13.54 ([d923bf3](https://gitlab.com/appframework/egore-personal/sms-simulator/commit/d923bf31411b262465bee68ff523368393dbd3c8))
* **deps:** update quarkus.platform.version to v3.17.8 ([7bedb2f](https://gitlab.com/appframework/egore-personal/sms-simulator/commit/7bedb2f0e2967fb0c1e5052dc5c0489d1247d909))
* **deps:** update quarkus.platform.version to v3.18.1 ([dc563dd](https://gitlab.com/appframework/egore-personal/sms-simulator/commit/dc563dd272f65391d0b4dd20775196120cb6047f))
* **deps:** update quarkus.platform.version to v3.18.2 ([c47759e](https://gitlab.com/appframework/egore-personal/sms-simulator/commit/c47759e8217a464ec99bb5727fa5bb79ac83a451))

## [1.0.12](https://gitlab.com/appframework/egore-personal/sms-simulator/compare/1.0.11...1.0.12) (2025-01-22)


### Bug Fixes

* **deps:** update quarkus.platform.version to v3.17.7 ([19325e1](https://gitlab.com/appframework/egore-personal/sms-simulator/commit/19325e1c5eacba177ca77b9682d4194895ce3fc5))

## [1.0.11](https://gitlab.com/appframework/egore-personal/sms-simulator/compare/1.0.10...1.0.11) (2025-01-15)


### Bug Fixes

* **deps:** update dependency com.googlecode.libphonenumber:libphonenumber to v8.13.53 ([f35f105](https://gitlab.com/appframework/egore-personal/sms-simulator/commit/f35f1054245f09498ca375faaf4dcd68dff087ff))
* **deps:** update quarkus.platform.version to v3.17.6 ([781bfc0](https://gitlab.com/appframework/egore-personal/sms-simulator/commit/781bfc0339676adb902c111aba09e43678e1e57e))

## [1.0.10](https://gitlab.com/appframework/egore-personal/sms-simulator/compare/1.0.9...1.0.10) (2024-12-25)


### Bug Fixes

* **deps:** update quarkus.platform.version to v3.17.5 ([79c92c8](https://gitlab.com/appframework/egore-personal/sms-simulator/commit/79c92c883b9cf8026b0c12ce7b46705f967dd035))

## [1.0.9](https://gitlab.com/appframework/egore-personal/sms-simulator/compare/1.0.8...1.0.9) (2024-12-13)


### Bug Fixes

* **deps:** update dependency com.googlecode.libphonenumber:libphonenumber to v8.13.52 ([24ca05a](https://gitlab.com/appframework/egore-personal/sms-simulator/commit/24ca05a1b18d7eb974d5853e1ff0ecb3ea2bb201))

## [1.0.8](https://gitlab.com/appframework/egore-personal/sms-simulator/compare/1.0.7...1.0.8) (2024-12-06)

## [1.0.7](https://gitlab.com/appframework/egore-personal/sms-simulator/compare/1.0.6...1.0.7) (2024-12-05)


### Bug Fixes

* **deps:** update dependency com.googlecode.libphonenumber:libphonenumber to v8.13.51 ([14605c9](https://gitlab.com/appframework/egore-personal/sms-simulator/commit/14605c9f8aa58d894c46248ec28f079e6adf7be3))

## [1.0.6](https://gitlab.com/appframework/egore-personal/sms-simulator/compare/1.0.5...1.0.6) (2024-11-27)

## [1.0.5](https://gitlab.com/appframework/egore-personal/sms-simulator/compare/1.0.4...1.0.5) (2024-11-20)

## [1.0.4](https://gitlab.com/appframework/egore-personal/sms-simulator/compare/1.0.3...1.0.4) (2024-11-15)


### Bug Fixes

* **deps:** update dependency com.googlecode.libphonenumber:libphonenumber to v8.13.50 ([baa902e](https://gitlab.com/appframework/egore-personal/sms-simulator/commit/baa902ead6bcd4a941721d5e345609a98c4b0b41))

## [1.0.3](https://gitlab.com/appframework/egore-personal/sms-simulator/compare/1.0.2...1.0.3) (2024-11-07)

## [1.0.2](https://gitlab.com/appframework/egore-personal/sms-simulator/compare/1.0.1...1.0.2) (2024-11-06)


### Bug Fixes

* **deps:** update dependency com.googlecode.libphonenumber:libphonenumber to v8.13.49 ([454dd91](https://gitlab.com/appframework/egore-personal/sms-simulator/commit/454dd91d0422bb04bf9a1c3647a0fe8c01223d3e))

## [1.0.1](https://gitlab.com/appframework/egore-personal/sms-simulator/compare/v1.0.0...1.0.1) (2024-10-22)


### Bug Fixes

* **deps:** update dependency com.googlecode.libphonenumber:libphonenumber to v8.13.48 ([d3403d2](https://gitlab.com/appframework/egore-personal/sms-simulator/commit/d3403d27d2b9f56f8257d2c9d64a8c1162b80591))

# 1.0.0 (2024-10-09)


### Bug Fixes

* **deps:** update dependency com.googlecode.libphonenumber:libphonenumber to v8.13.35 ([05936e7](https://gitlab.com/appframework/egore-personal/sms-simulator/commit/05936e70c70f853b39933c7269e691a07e1f28d9))
* **deps:** update dependency com.googlecode.libphonenumber:libphonenumber to v8.13.36 ([03448a7](https://gitlab.com/appframework/egore-personal/sms-simulator/commit/03448a7131ddd3e3b7b6d3a2b5f2242d2baab727))
* **deps:** update dependency com.googlecode.libphonenumber:libphonenumber to v8.13.37 ([8cb2513](https://gitlab.com/appframework/egore-personal/sms-simulator/commit/8cb2513226f95a5eca382f93ace5f3adc204b5a2))
* **deps:** update dependency com.googlecode.libphonenumber:libphonenumber to v8.13.38 ([e75d678](https://gitlab.com/appframework/egore-personal/sms-simulator/commit/e75d678efef7b53112d98e43a62c0fda82ad0718))
* **deps:** update dependency com.googlecode.libphonenumber:libphonenumber to v8.13.39 ([026518a](https://gitlab.com/appframework/egore-personal/sms-simulator/commit/026518a47e171ab8f0b3d36de4370465ea9da545))
* **deps:** update dependency com.googlecode.libphonenumber:libphonenumber to v8.13.40 ([0fafef9](https://gitlab.com/appframework/egore-personal/sms-simulator/commit/0fafef9119ac635e840ee5754f954f229b398b45))
* **deps:** update dependency com.googlecode.libphonenumber:libphonenumber to v8.13.42 ([d96985f](https://gitlab.com/appframework/egore-personal/sms-simulator/commit/d96985fa91790b233c298816c6218a9092ae7438))
* **deps:** update dependency com.googlecode.libphonenumber:libphonenumber to v8.13.43 ([1d7e6fb](https://gitlab.com/appframework/egore-personal/sms-simulator/commit/1d7e6fb421820fbb7d916f8c52d1d2f3f762ec65))
* **deps:** update dependency com.googlecode.libphonenumber:libphonenumber to v8.13.44 ([68d04ad](https://gitlab.com/appframework/egore-personal/sms-simulator/commit/68d04ad59159d62e39f1e1da0a3c52a07d389002))
* **deps:** update dependency com.googlecode.libphonenumber:libphonenumber to v8.13.45 ([bedbee9](https://gitlab.com/appframework/egore-personal/sms-simulator/commit/bedbee9b5da8b28915651918e31314ccfa37939c))
* **deps:** update dependency com.googlecode.libphonenumber:libphonenumber to v8.13.46 ([aae6c29](https://gitlab.com/appframework/egore-personal/sms-simulator/commit/aae6c29a4af262ca2563e047cb037e2bf7212022))
* **deps:** update dependency com.googlecode.libphonenumber:libphonenumber to v8.13.47 ([dca8642](https://gitlab.com/appframework/egore-personal/sms-simulator/commit/dca86421de4f35106b55a039e16e945fcd8076b0))
* **deps:** update dependency org.ocpsoft.prettytime:prettytime to v5.0.8.final ([e02e167](https://gitlab.com/appframework/egore-personal/sms-simulator/commit/e02e167ad7bebd499c2512645ce8d50c1f70eb0d))
* **deps:** update dependency org.ocpsoft.prettytime:prettytime to v5.0.9.final ([f16370f](https://gitlab.com/appframework/egore-personal/sms-simulator/commit/f16370f76272bc9c10844544222ed20936c3d3f7))
* **messages:** Only retrieve messages for the current recipient ([3b4f2b8](https://gitlab.com/appframework/egore-personal/sms-simulator/commit/3b4f2b8a5dabfa8d7177c93cf9e1bf0f7bdf1921))


### Features

* **ci:** Add default pipeline ([69873b6](https://gitlab.com/appframework/egore-personal/sms-simulator/commit/69873b667bb7190253ef23489c58f02d5475e597))
* **docs:** Add README.md ([596b141](https://gitlab.com/appframework/egore-personal/sms-simulator/commit/596b14193948a32bf5841525e354efdb3477c72e))
* **message:** Don't highlight the link in color ([8929e21](https://gitlab.com/appframework/egore-personal/sms-simulator/commit/8929e212eed69b8430281f092ee289d3569017d7))
