# SMS simulator

This projects implements third party SMS services to display them in the browser
for testing purposes. It mocks a phone UI to display the messages received.

## Accessing the SMS per recipient

The URL to display all senders that sent messages to a recipient open
`/{recipient}`. The list of recipients is displayed in alphabetical order.

![Overview](docs/sms-simulator-overview.png)

Upon clicking on any of the recipients you will see the messages send by the
sender to the selected recipient. You can also directly access this at
`/{recipient}/{sender}`. The most recent messages are displayed at the bottom.

![Messages](docs/sms-simulator-messages.png)

## Receiving messages

Currently, the following SMS services are implemented:

- ClickSend
- CM.com
- gatewayapi

Adding further providers is possible. See `de.egore911.smssimulator.services`
for how the existing ones are implemented. Merge requests are welcome to add
additional APIs.

## Creating mock messages

For testing purposes you can also create mocked messages, which can be used for
showcasing the functionality of this software. Two types are supported:

- Single: When clicking on the `+` you will get a single new message. If you are
  on the message list, it will create one for the currently selected sender.
  Otherwise, the sender will be "mock".
- Multiple: When clicking the `++` you will get ten different senders from A to
  J sending you the same SMS.
