package de.egore911.smssimulator;

import de.egore911.smssimulator.services.GatewayAPI;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@QuarkusTest
@TestHTTPEndpoint(GatewayAPI.class)
class GatewayAPITest {

    @ConfigProperty(name = "sms.api-key")
    String apiKey;

    @Test
    void testSendingOneSmsToTwoRecipient() throws IOException {
        given()
                .header("Authorization", "Basic " + Base64.getEncoder().encodeToString(apiKey.getBytes(StandardCharsets.UTF_8)))
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "    \"message\": \"Hello World\",\n" +
                        "    \"recipients\": [\n" +
                        "        {\"msisdn\": 4512345678},\n" +
                        "        {\"msisdn\": 4587654321}\n" +
                        "    ]\n" +
                        "}")
                .when()
                .post()
                .then()
                .statusCode(200)
                .body("usage.currency", equalTo("EUR"))
                .body("usage.countries.DE", equalTo(2))
                .body("usage.total_cost", equalTo(0.1f));
    }
}
