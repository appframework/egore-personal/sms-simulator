package de.egore911.smssimulator;

import de.egore911.smssimulator.services.Clicksend;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@QuarkusTest
@TestHTTPEndpoint(Clicksend.class)
class ClicksendTest {

    @ConfigProperty(name = "sms.api-key")
    String apiKey;

    @Test
    void testSendingOneSmsToTwoRecipient() throws IOException {
        given()
                .header("Authorization", "Basic " + Base64.getEncoder().encodeToString(apiKey.getBytes(StandardCharsets.UTF_8)))
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "    \"messages\":[\n" +
                        "        {\n" +
                        "            \"source\":\"php\",\n" +
                        "            \"body\":\"Jelly liquorice marshmallow candy carrot cake 4Eyffjs1vL.\",\n" +
                        "            \"to\":\"+61411111111\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"source\":\"php\",\n" +
                        "            \"body\":\"Chocolate bar icing icing oat cake carrot cake jelly cotton MWEvciEPIr.\",\n" +
                        "            \"to\":\"+61422222222\"\n" +
                        "        }\n" +
                        "    ]\n" +
                        "}")
                .when()
                .post()
                .then()
                .statusCode(200)
                .body("data._currency.currency_name_short", equalTo("EUR"))
                .body("data.total_price", equalTo(0.1f));
    }
}
