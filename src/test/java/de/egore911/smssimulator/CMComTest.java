package de.egore911.smssimulator;

import com.cm.text.Config;
import com.cm.text.MessagingClient;
import de.egore911.smssimulator.services.CMCom;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.common.http.TestHTTPResource;
import io.quarkus.test.junit.QuarkusTest;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URL;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@QuarkusTest
@TestHTTPEndpoint(CMCom.class)
class CMComTest {

    @ConfigProperty(name = "sms.api-key")
    String apiKey;

    @TestHTTPEndpoint(CMCom.class)
    @TestHTTPResource
    URL serviceUrl;

    @Test
    void testSendingSms() throws IOException {
        Config.BusinessMessagingApiUrl = serviceUrl.toString();

        var client = new MessagingClient(apiKey);

        var response = client.sendTextMessage("Message Text", "TestSender", new String[] {"00316012345678"});
        assertThat(response, notNullValue());
        assertThat(response.getMessages().length, equalTo(1));
        assertThat(response.getDetails(), equalTo("Created 1 message(s)"));

        var response2 = client.sendTextMessage("Message Text", "TestSender", new String[] {"00316012345678", "00123456789"});
        assertThat(response2, notNullValue());
        assertThat(response2.getMessages().length, equalTo(2));
        assertThat(response2.getDetails(), equalTo("Created 2 message(s)"));

        var response3 = client.sendTextMessage("This is an unimportant text that is lengthy and verbose enough, so it will cause exactly two SMS to be sent. At least if I did everything right and could count to 160.", "TestSender", new String[] {"00316012345678"});
        assertThat(response3, notNullValue());
        assertThat(response3.getMessages().length, equalTo(1));
        assertThat(response3.getDetails(), equalTo("Created 2 message(s)"));

    }
}
