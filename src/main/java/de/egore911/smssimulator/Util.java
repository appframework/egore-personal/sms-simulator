package de.egore911.smssimulator;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import io.quarkus.logging.Log;
import jakarta.annotation.Nonnull;
import jakarta.enterprise.context.ApplicationScoped;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@ApplicationScoped
public class Util {

    @ConfigProperty(name = "sms.default-country", defaultValue = "DE")
    String defaultCountry;

    // Stolen from https://stackoverflow.com/questions/3809401/what-is-a-good-regular-expression-to-match-a-url#3809435
    private static final Pattern LINK_PATTERN = Pattern.compile("(https?://(www\\.)?[-a-zA-Z0-9@:%._+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_+.~#?&/=]*))");

    public String normalize(@Nonnull String number) {
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        try {
            Phonenumber.PhoneNumber parsed = phoneUtil.parse(number, defaultCountry);
            return phoneUtil.format(parsed, PhoneNumberUtil.PhoneNumberFormat.E164);
        } catch (NumberParseException e) {
            Log.debugv("Failed to parse numer {0}", number, e);
        }
        return number;
    }

    public Main.Color fakeColor(@Nonnull String input) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-1");
            byte[] hashBytes = digest.digest(input.getBytes(StandardCharsets.UTF_8));
            StringBuilder hexString = new StringBuilder();
            int sum = 0;
            for (byte b : hashBytes) {
                sum += 0xff & b;
                String hex = Integer.toHexString(0xff & b);
                if (hex.length() == 1) {
                    hexString.append('0');
                }
                hexString.append(hex);
                if (hexString.length() >= 6) {
                    break;
                }
            }
            return new Main.Color("#" + hexString, sum < (3 * 128) ? "#ffffff" : "#000000");
        } catch (NoSuchAlgorithmException e) {
            // Return red color in case of algorithm not found
            return new Main.Color("#ff0000", "#000000");
        }
    }

    public String abbreviate(@Nonnull String input) {
        if (input.length() < 42) {
            return input;
        }
        return input.substring(0, 40) + "...";
    }

    public String linkify(@Nonnull String input) {
        var matcher = LINK_PATTERN.matcher(input);
        StringBuilder sb = new StringBuilder();
        while (matcher.find()) {
            String link = matcher.group(1);
            matcher.appendReplacement(sb, "<a target=\"_blank\" href=\"" + link + "\">" + link + "</a>");
        }
        matcher.appendTail(sb);
        return sb.toString();
    }
}
