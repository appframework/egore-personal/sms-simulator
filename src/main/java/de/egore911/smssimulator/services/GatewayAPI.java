package de.egore911.smssimulator.services;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import de.egore911.smssimulator.SMS;
import io.quarkus.hibernate.reactive.panache.common.WithTransaction;
import io.quarkus.logging.Log;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import java.time.LocalDateTime;
import java.util.*;

/**
 * https://gatewayapi.com/docs/apis/rest/
 */
@Path("/rest/mtsms")
public class GatewayAPI {


    @ConfigProperty(name = "sms.api-key")
    String apiKey;
    @ConfigProperty(name = "sms.default-country", defaultValue = "DE")
    String defaultCountry;
    @ConfigProperty(name = "sms.default-sender", defaultValue = "SMS Simulator")
    String defaultSender;

    public static class Body {
        @JsonProperty("class")
        public String class_;
        public String message;
        public String sender;
        public String userref;
        public String callback_url;
        public List<Recipient> recipients;
    }

    public static class Recipient {
        public String msisdn;
    }

    public static class ResponseContent {
        public List<Long> ids = new ArrayList<>();
        public Usage usage = new Usage();
    }

    public static class Usage {
        public Map<String, Long> countries = new HashMap<>();
        public String currency = "EUR";
        @JsonProperty("total_cost")
        public float totalCost;
    }

    @POST
    @WithTransaction
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<Response> mtsms(@HeaderParam("Authorization") String authorization, Body body) {

        if (body == null) {
            return Uni.createFrom()
                    .item(Response.status(400)
                            .build());
        }

        if (authorization == null || authorization.isEmpty()) {
            return Uni.createFrom()
                    .item(Response.status(401)
                            .build());
        } else {
            if (!apiKey.equals(new String(Base64.getDecoder().decode(authorization.substring(6))))) {
                return Uni.createFrom()
                        .item(Response.status(401)
                                .build());
            }
        }


        return Uni.createFrom()
                .item(ResponseContent::new)
                .call((response -> Multi.createFrom()
                        .iterable(body.recipients)

                        // Create and persist an SMS per recipient
                        .onItem().transformToUniAndConcatenate(recipient -> {
                            var sms = new SMS();
                            PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
                            String country = defaultCountry;
                            try {
                                Phonenumber.PhoneNumber parsed = phoneUtil.parse(recipient.msisdn, defaultCountry);
                                sms.recipient = phoneUtil.format(parsed, PhoneNumberUtil.PhoneNumberFormat.E164);
                                country = phoneUtil.getRegionCodeForCountryCode(parsed.getCountryCode());
                            } catch (NumberParseException e) {
                                Log.warnv("Failed to parse numer {0}", recipient.msisdn, e);
                                sms.recipient = recipient.msisdn;
                            }
                            Map<String, Long> map = response.usage.countries;
                            var percountry = map.getOrDefault(country, 0L);
                            percountry += (body.message.length() / 160) + 1;
                            map.put(country, percountry);
                            sms.country = country;
                            sms.message = body.message;
                            sms.sender = body.sender != null ? body.sender : defaultSender;
                            sms.received = LocalDateTime.now();
                            sms.service = "GatewayAPI";

                            return sms.<SMS>persist();
                        })

                        // Map to IDs of the stored SMS
                        .map(sms -> sms.id)
                        .collect()
                        .asList()

                        // Put the relevant information into the return value
                        .map(ids -> {
                            response.ids = ids;
                            response.usage.totalCost = response.usage.countries.values()
                                    .stream()
                                    .mapToLong(Long::longValue)
                                    .sum() * 0.05f;
                            return response;
                        })))
                .map((responseContent -> Response.ok()
                        .entity(responseContent)
                        .build()));
    }
}
