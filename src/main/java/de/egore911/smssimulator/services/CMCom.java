package de.egore911.smssimulator.services;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import de.egore911.smssimulator.SMS;
import io.quarkus.hibernate.reactive.panache.common.WithTransaction;
import io.quarkus.logging.Log;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import jakarta.annotation.Nonnull;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * https://developers.cm.com/messaging/docs/sms
 */
@Path("/v1.0/message")
public class CMCom {

    @ConfigProperty(name = "sms.default-country", defaultValue = "DE")
    String defaultCountry;
    @ConfigProperty(name = "sms.default-sender", defaultValue = "SMS Simulator")
    String defaultSender;

    public static class Body {
        public Messages messages;
    }

    public static class Messages {
        public Authentication authentication;
        public List<Message> msg;
    }

    public static class Authentication {
        public String producttoken;
    }

    public static class Message {
        public MessageBody body;
        public String customGrouping;
        public String from;
        public List<Recipient> to;
        public Long dcs;
    }

    public static class MessageBody {
        public String content;
    }

    public static class Recipient {
        public String number;
    }

    public static class ResponseContent {
        public String details;
        public long errorCode;
        public List<SentMessage> messages = new ArrayList<>();
    }

    public static class SentMessage {
        public String to;
        public String status;
        public String reference;
        public long parts;
        public String messageDetails;
        public long messageErrorCode;
    }

    @POST
    @WithTransaction
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<ResponseContent> message(@Nonnull Body body) {
        return Uni.createFrom()
                .item(ResponseContent::new)
                .call((response -> Multi.createFrom()
                        .iterable(body.messages.msg)

                        // Loop over all the SMS
                        .onItem().transformToUniAndConcatenate(message -> Multi.createFrom()
                                .iterable(message.to)

                                // Create and persist an SMS per recipient
                                .onItem().transformToUniAndConcatenate(recipient -> {
                                    var sms = new SMS();
                                    PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
                                    String country = defaultCountry;
                                    try {
                                        Phonenumber.PhoneNumber parsed = phoneUtil.parse(recipient.number, defaultCountry);
                                        sms.recipient = phoneUtil.format(parsed, PhoneNumberUtil.PhoneNumberFormat.E164);
                                        country = phoneUtil.getRegionCodeForCountryCode(parsed.getCountryCode());
                                    } catch (NumberParseException e) {
                                        Log.warnv("Failed to parse numer {0}", recipient.number, e);
                                        sms.recipient = recipient.number;
                                    }
                                    sms.country = country;
                                    sms.message = message.body.content;
                                    sms.sender = message.from != null ? message.from : defaultSender;
                                    sms.received = LocalDateTime.now();
                                    sms.service = "cm.com";

                                    return sms.<SMS>persist();
                                })

                                // Create an SentMessage entry for each stored SMS
                                .map(sms -> {
                                    SentMessage sent = new SentMessage();
                                    sent.messageErrorCode = 0;
                                    sent.parts = (sms.message.length() / 160) + 1L;
                                    sent.status = "Accepted";
                                    return sent;
                                })

                                // Put all into the response object
                                .collect()
                                .asList()
                                .map(sent -> {
                                    response.messages = sent;
                                    response.details = MessageFormat.format("Created {0} message(s)", response.messages.stream().mapToLong(s -> s.parts).sum());
                                    return response;
                                })).toUni()
                ));
    }
}
