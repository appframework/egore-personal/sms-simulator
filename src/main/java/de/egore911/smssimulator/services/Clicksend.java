package de.egore911.smssimulator.services;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import de.egore911.smssimulator.SMS;
import io.quarkus.hibernate.reactive.panache.common.WithTransaction;
import io.quarkus.logging.Log;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import java.time.LocalDateTime;
import java.util.*;

/**
 * https://developers.clicksend.com/docs/rest/v3/?java#send-sms
 */
@Path("/v3/sms/send")
public class Clicksend {

    @ConfigProperty(name = "sms.api-key")
    String apiKey;
    @ConfigProperty(name = "sms.default-country", defaultValue = "DE")
    String defaultCountry;
    @ConfigProperty(name = "sms.default-sender", defaultValue = "SMS Simulator")
    String defaultSender;

    public static class Body {
        public List<Message> messages;
    }

    public static class Message {
        public String source;
        public String body;
        public String to;
    }

    public static class ResponseContent {
        @JsonProperty("_http_code")
        public int httpCode = 200;
        @JsonProperty("response_code")
        public String response = "SUCCESS";
        @JsonProperty("response_msg")
        public String message;
        public ResponseData data = new ResponseData();
    }

    public static class ResponseData {
        @JsonProperty("total_price")
        public double totalPrice;
        @JsonProperty("total_count")
        public long totalCount;
        @JsonProperty("queued_count")
        public long queuedCount;
        public List<SentMessage> messages = new ArrayList<>();
        @JsonProperty("_currency")
        public Currency currency = new Currency();
    }

    public static class SentMessage {
        public String direction = "out";
        public long date;
        public String to;
        public String body;
        public String from;
        public long schedule;
        @JsonProperty("message_id")
        public String messageId;
        @JsonProperty("message_parts")
        public long parts;
        @JsonProperty("message_price")
        public float price;
        @JsonProperty("custom_string")
        public String customString;
        @JsonProperty("user_id")
        public long userId = 1;
        @JsonProperty("subaccount_id")
        public long subaccountId = 1;
        public String country;
        public String carrier = "Unknown";
        public String status = "SUCCESS";
    }

    public static class Currency {
        @JsonProperty("currency_name_short")
        String name = "EUR";
        @JsonProperty("currency_prefix_d")
        String prefix = "€";
        @JsonProperty("currency_prefix_c")
        String prefixMinor = "¢";
        @JsonProperty("currency_name_long")
        String nameLong = "Euro";
    }

    @POST
    @WithTransaction
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<Response> mtsms(@HeaderParam("Authorization") String authorization, Body body) {

        if (authorization == null || authorization.isEmpty()) {
            return Uni.createFrom()
                    .item(Response.status(401)
                            .build());
        } else {
            if (!apiKey.equals(new String(Base64.getDecoder().decode(authorization.substring(6))))) {
                return Uni.createFrom()
                        .item(Response.status(401)
                                .build());
            }
        }


        return Uni.createFrom()
                .item(ResponseContent::new)
                .call((response -> Multi.createFrom()
                        .iterable(body.messages)

                        // Create and persist an SMS per recipient
                        .onItem().transformToUniAndConcatenate(message -> {
                            var sms = new SMS();
                            PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
                            String country = defaultCountry;
                            try {
                                Phonenumber.PhoneNumber parsed = phoneUtil.parse(message.to, defaultCountry);
                                sms.recipient = phoneUtil.format(parsed, PhoneNumberUtil.PhoneNumberFormat.E164);
                                country = phoneUtil.getRegionCodeForCountryCode(parsed.getCountryCode());
                            } catch (NumberParseException e) {
                                Log.warnv("Failed to parse numer {0}", message.to, e);
                                sms.recipient = message.to;
                            }
                            sms.country = country;
                            sms.message = message.body;
                            sms.sender = message.source != null ? message.source : defaultSender;
                            sms.received = LocalDateTime.now();
                            sms.service = "Clicksend";

                            return sms.<SMS>persist();
                        })

                        // Put the relevant information into the return value
                        .map(sms -> {
                            var sent = new SentMessage();

                            sent.date = sent.schedule = System.currentTimeMillis() / 1000L;
                            sent.to = sms.recipient;
                            sent.body = sms.message;
                            sent.from = sms.sender;
                            sent.messageId = new UUID(1234567, sms.id).toString().toUpperCase(Locale.ROOT);
                            sent.parts = (sms.message.length() / 160) + 1L;
                            sent.price = sent.parts * 0.05f;
                            sent.country = sms.country;
                            sent.status = "SUCCESS";
                           return sent;
                        })
                        .collect().asList()
                        .map(sents -> {
                            response.data.messages = sents;
                            response.data.totalCount = response.data.messages.stream().mapToLong(m -> m.parts).sum();
                            response.data.totalPrice = response.data.messages.stream().mapToDouble(m -> m.price).sum();
                            return response;
                        })))
                .map((responseContent -> Response.ok()
                        .entity(responseContent)
                        .build()));
    }
}
