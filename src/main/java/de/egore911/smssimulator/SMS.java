package de.egore911.smssimulator;

import io.quarkus.hibernate.reactive.panache.PanacheEntity;
import jakarta.persistence.Entity;
import jakarta.validation.constraints.NotBlank;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDateTime;

@Entity
public class SMS extends PanacheEntity {

    @NotBlank(message = "A recipient is required")
    public String recipient;
    @NotBlank(message = "A sender is required")
    @Length(max = 13, message = "A maximum of 13 characters is allowed for the sender")
    public String sender;
    @CreationTimestamp
    public LocalDateTime received;
    @NotBlank(message = "A message is required")
    public String message;
    @NotBlank(message = "A service provider should be automatically set during sending")
    public String service;
    public String country;

}
