package de.egore911.smssimulator;

import io.quarkus.hibernate.reactive.panache.common.WithTransaction;
import io.quarkus.logging.Log;
import io.quarkus.panache.common.Sort;
import io.quarkus.qute.CheckedTemplate;
import io.quarkus.qute.Template;
import io.quarkus.qute.TemplateInstance;
import io.smallrye.mutiny.Uni;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import org.hibernate.reactive.mutiny.Mutiny;
import org.ocpsoft.prettytime.PrettyTime;

import java.util.ArrayList;
import java.util.List;

@Path("/")
public class Main {

    @CheckedTemplate
    public static class Templates {
        public static native TemplateInstance messagelist(String recipientNumber, List<Message> messages);
        public static native TemplateInstance senderlist(String recipientNumber, List<Sender> senders);
    }

    @Inject
    Util util;

    @Inject
    Mutiny.SessionFactory sessionFactory;

    public static class Color {
        public String background;
        public String foreground;

        public Color(String background, String foreground) {
            this.background = background;
            this.foreground = foreground;
        }
    }

    public static class Sender {
        public String name;
        public String latestMessage;
        public Color color;

        public Sender(String name, String latestMessage, Color color) {
            this.name = name;
            this.latestMessage = latestMessage;
            this.color = color;
        }
    }

    public static class Message {
        public String received;
        public String text;

        public Message(String received, String text) {
            this.received = received;
            this.text = text;
        }
    }

    @GET
    @Path("/{recipient}")
    @Produces(MediaType.TEXT_HTML)
    public Uni<TemplateInstance> getMessages(@PathParam("recipient") String recipient) {
        String normalizedRecipient = util.normalize(recipient);

        return sessionFactory
                .withSession(s -> s.createNativeQuery("select s1.sender, s1.message from SMS s1 join (select sender, max(received) as received from SMS where recipient = ?1 group by sender) s2 on s1.sender = s2.sender and s1.received = s2.received order by sender", Object[].class).setParameter(1, recipient).getResultList())
                .map(senders -> senders.stream().map((sender -> {
                    String name = (String) sender[0];
                    Color color = util.fakeColor(name);
                    return new Sender(name, util.abbreviate((String) sender[1]), color);
                })).toList())
                .map(senders -> Templates.senderlist(normalizedRecipient, senders));
    }

    @GET
    @Path("/{recipient}/{sender}")
    @Produces(MediaType.TEXT_HTML)
    public Uni<TemplateInstance> getMessages(@PathParam("recipient") String recipient,
                                             @PathParam("sender") String sender) {
        String normalizedRecipient = util.normalize(recipient);
        String normalizedSender = util.normalize(sender);

        return SMS.<SMS>list("recipient = ?1 and sender = ?2", Sort.ascending("received"), normalizedRecipient, normalizedSender)
                .map(smss -> {
                    Log.infov("Retrieved {0} sms for number {1}", smss.size(), normalizedRecipient);
                    List<Message> messages = new ArrayList<>();
                    var prettytime = new PrettyTime();
                    for (SMS sms : smss) {
                        messages.add(new Message(prettytime.format(sms.received), util.linkify(sms.message)));
                    }
                    return Templates.messagelist(normalizedRecipient, messages);
                });

    }

    @POST
    @Path("/{recipient}")
    @WithTransaction
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<SMS> mock(@PathParam("recipient") String recipient) {
        String normalizedRecipient = util.normalize(recipient);

        var sms = new SMS();
        sms.recipient = normalizedRecipient;
        sms.sender = "mock";
        sms.message = "This is a mocked message created by clicking the button in the UI.";
        sms.service = "Mock";
        return sms.persist();
    }

    @POST
    @Path("/{recipient}/{sender}")
    @WithTransaction
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<SMS> mock(@PathParam("recipient") String recipient,
                         @PathParam("sender") String sender) {
        String normalizedRecipient = util.normalize(recipient);
        String normalizedSender = util.normalize(sender);

        var sms = new SMS();
        sms.recipient = normalizedRecipient;
        sms.sender = normalizedSender;
        sms.message = "This is a mocked message created by clicking the button in the UI. It contains a link to https://www.christophbrill.de/";
        sms.service = "Mock";
        return sms.persist();
    }

}
